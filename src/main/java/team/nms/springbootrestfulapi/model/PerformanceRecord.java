package team.nms.springbootrestfulapi.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("performanceRecords")
@CompoundIndexes(
        @CompoundIndex(name = "sid_year", def = "{'sid': 1, 'year': 1}", unique = true)
)
public class PerformanceRecord {

    @Id
    private String id;

    private int sid;

    private int year;

    private String description;

    private int targetValue;

    private int actualValue;

    public PerformanceRecord(int sid, int year, String description, int targetValue, int actualValue) {
        this.sid = sid;
        this.year = year;
        this.description = description;
        this.targetValue = targetValue;
        this.actualValue = actualValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(int targetValue) {
        this.targetValue = targetValue;
    }

    public int getActualValue() {
        return actualValue;
    }

    public void setActualValue(int actualValue) {
        this.actualValue = actualValue;
    }

    @Override
    public String toString() {
        return "PerformanceRecord{" +
                "id='" + id + '\'' +
                ", sid=" + sid +
                ", year=" + year +
                ", description='" + description + '\'' +
                ", targetValue=" + targetValue +
                ", actualValue=" + actualValue +
                '}';
    }
}
