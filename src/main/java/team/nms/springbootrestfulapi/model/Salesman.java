package team.nms.springbootrestfulapi.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("salesmen")
public class Salesman {
    @Id
    private String id;

    @Indexed(unique = true)
    private int sid;

    private String fullname;

    private String department;

    public Salesman(int sid, String fullname, String department) {
        this.sid = sid;
        this.fullname = fullname;
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Salesman{" +
                "id='" + id + '\'' +
                ", sid=" + sid +
                ", fullname='" + fullname + '\'' +
                ", department='" + department + '\'' +
                '}';
    }
}
