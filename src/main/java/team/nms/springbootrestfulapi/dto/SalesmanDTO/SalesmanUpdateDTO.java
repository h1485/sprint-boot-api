package team.nms.springbootrestfulapi.dto.SalesmanDTO;

public class SalesmanUpdateDTO {
    private Integer sid;
    private String fullname;
    private String department;

    public SalesmanUpdateDTO(Integer sid, String fullname, String department) {
        this.sid = sid;
        this.fullname = fullname;
        this.department = department;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
