package team.nms.springbootrestfulapi.dto.SalesmanDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SalesmanCreateDTO {

    @NotNull(message = "Please enter salesman ID")
    @Min(0)
    private Integer sid;

    @NotBlank(message = "Please enter a name!")
    private String fullname;

    private String department;

    public SalesmanCreateDTO(Integer sid, String fullname, String department) {
        this.sid = sid;
        this.fullname = fullname;
        this.department = department;
    }


    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

}
