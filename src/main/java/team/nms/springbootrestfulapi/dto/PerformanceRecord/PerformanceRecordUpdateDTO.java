package team.nms.springbootrestfulapi.dto.PerformanceRecord;

import javax.validation.constraints.Min;

public class PerformanceRecordUpdateDTO {

    private Integer sid;

    private Integer year;

    private String description;

    private Integer targetValue;

    private Integer actualValue;

    public PerformanceRecordUpdateDTO(Integer sid, Integer year, String description, Integer targetValue, Integer acutalValue) {
        this.sid = sid;
        this.year = year;
        this.description = description;
        this.targetValue = targetValue;
        this.actualValue = acutalValue;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(Integer targetValue) {
        this.targetValue = targetValue;
    }

    public Integer getActualValue() {
        return actualValue;
    }

    public void setActualValue(Integer actualValue) {
        this.actualValue = actualValue;
    }
}
