package team.nms.springbootrestfulapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import team.nms.springbootrestfulapi.model.PerformanceRecord;

import java.util.List;
import java.util.Optional;

@Repository
public interface PerformanceRecordRepository extends MongoRepository<PerformanceRecord, String> {
    List<PerformanceRecord> findBySid(int sid);
    Optional<PerformanceRecord> findBySidAndYear(int sid, int year);
    void deleteBySidAndYear(int sid, int year);
}
