package team.nms.springbootrestfulapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import team.nms.springbootrestfulapi.model.Salesman;

import java.util.Optional;

@Repository
public interface SalesmanRepository extends MongoRepository<Salesman, String> {
    Optional<Salesman> findBySid(int sid);
    void deleteBySid(int sid);
}
