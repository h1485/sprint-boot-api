package team.nms.springbootrestfulapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import team.nms.springbootrestfulapi.dto.SalesmanDTO.SalesmanCreateDTO;
import team.nms.springbootrestfulapi.dto.SalesmanDTO.SalesmanUpdateDTO;
import team.nms.springbootrestfulapi.model.Salesman;
import team.nms.springbootrestfulapi.repository.SalesmanRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/salesman")
public class SalesmanController {

    @Autowired
    SalesmanRepository salesmanRepository;

    @GetMapping
    public ResponseEntity<List<Salesman>> querySalesmen() {
        List<Salesman> salesmen = salesmanRepository.findAll();
        return new ResponseEntity<>(salesmen, HttpStatus.OK);
    }

    @GetMapping("/{sid}")
    public ResponseEntity<Salesman> readSalesman(@PathVariable("sid") int sid) {
        Optional<Salesman> salesManData = salesmanRepository.findBySid(sid);

        if (salesManData.isPresent()) {
            return new ResponseEntity<>(salesManData.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Salesman> createSalesman(@Valid @RequestBody SalesmanCreateDTO salesmanCreateDTO) {
        try {
            Salesman _s = salesmanRepository.save(
                    new Salesman(
                            salesmanCreateDTO.getSid(),
                            salesmanCreateDTO.getFullname(),
                            salesmanCreateDTO.getDepartment()
                    )
            );
            return new ResponseEntity<>(_s, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Maybe PATCH instead
    @PutMapping("/{sid}")
    public ResponseEntity<Salesman> updateSalesman(
            @PathVariable("sid") int sid,
            @RequestBody SalesmanUpdateDTO salesmanUpdateDTO
    ) {
        Optional<Salesman> salesmanData = salesmanRepository.findBySid(sid);

        if (salesmanData.isPresent()) {
            Salesman _salesman = salesmanData.get();
            if (salesmanUpdateDTO.getSid() != null) {
                _salesman.setSid(salesmanUpdateDTO.getSid());
            }
            if (salesmanUpdateDTO.getFullname() != null && !salesmanUpdateDTO.getFullname().equals("")) {
                _salesman.setFullname(salesmanUpdateDTO.getFullname());
            }
            if (salesmanUpdateDTO.getDepartment() != null) {
                _salesman.setDepartment(salesmanUpdateDTO.getDepartment());
            }
            return new ResponseEntity<>(salesmanRepository.save(_salesman), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{sid}")
    public ResponseEntity<HttpStatus> deleteSalesman(@PathVariable("sid") int sid) {
        Optional<Salesman> salesmanData = salesmanRepository.findBySid(sid);
        if (salesmanData.isPresent()) {
            try {
                salesmanRepository.deleteBySid(sid);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT); // 204
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
