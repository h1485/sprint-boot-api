package team.nms.springbootrestfulapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import team.nms.springbootrestfulapi.dto.PerformanceRecord.PerformanceRecordCreateDTO;
import team.nms.springbootrestfulapi.dto.PerformanceRecord.PerformanceRecordUpdateDTO;
import team.nms.springbootrestfulapi.model.PerformanceRecord;
import team.nms.springbootrestfulapi.model.Salesman;
import team.nms.springbootrestfulapi.repository.PerformanceRecordRepository;
import team.nms.springbootrestfulapi.repository.SalesmanRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/performancerecord")
public class PerformanceRecordController {

    @Autowired
    SalesmanRepository salesmanRepository;
    @Autowired
    PerformanceRecordRepository performanceRecordRepository;

    @GetMapping
    public ResponseEntity<List<PerformanceRecord>> queryPerformanceRecords() {
        List<PerformanceRecord> performanceRecords = performanceRecordRepository.findAll();
        return new ResponseEntity<>(performanceRecords, HttpStatus.OK);
    }

    @GetMapping("/{sid}")
    public ResponseEntity<List<PerformanceRecord>> getPerformanceRecordsBySid(@PathVariable("sid") int sid) {
        List<PerformanceRecord> performanceRecords = performanceRecordRepository.findBySid(sid);
        return new ResponseEntity<>(performanceRecords, HttpStatus.OK);
    }

    @GetMapping("/{sid}/{year}")
    public ResponseEntity<PerformanceRecord> getPerformanceRecord
            (
                    @PathVariable("sid") int sid,
                    @PathVariable("year") int year
            ) {
        Optional<PerformanceRecord> prData = performanceRecordRepository.findBySidAndYear(sid, year);

        if (prData.isPresent()) {
            return new ResponseEntity<>(prData.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<PerformanceRecord> createPerformanceRecord
            (@Valid @RequestBody PerformanceRecordCreateDTO prCreateDTO) {
        Optional<Salesman> SalesmanData = salesmanRepository.findBySid(prCreateDTO.getSid());

        if (SalesmanData.isPresent()) {
            try {
                PerformanceRecord _p = new PerformanceRecord(
                        prCreateDTO.getSid(),
                        prCreateDTO.getYear(),
                        prCreateDTO.getDescription(),
                        prCreateDTO.getTargetValue(),
                        prCreateDTO.getActualValue()
                );

                return new ResponseEntity<>(performanceRecordRepository.save(_p), HttpStatus.CREATED);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{sid}/{year}")
    public ResponseEntity<PerformanceRecord> updatePerformanceRecord
            (
                    @PathVariable("sid") int sid,
                    @PathVariable("year") int year,
                    @Valid @RequestBody PerformanceRecordUpdateDTO prUpdateDTO
            ) {

        Optional<PerformanceRecord> prData = performanceRecordRepository.findBySidAndYear(sid, year);

        if (prData.isPresent()) {
            PerformanceRecord _pr = prData.get();

            if (prUpdateDTO.getSid() != null) {
                _pr.setSid(prUpdateDTO.getSid());
            }

            if (prUpdateDTO.getYear() != null) {
                _pr.setYear(prUpdateDTO.getYear());
            }

            if (prUpdateDTO.getDescription() != null && !prUpdateDTO.getDescription().equals("")) {
                _pr.setDescription(prUpdateDTO.getDescription());
            }

            if (prUpdateDTO.getTargetValue() != null) {
                _pr.setTargetValue(prUpdateDTO.getTargetValue());
            }

            if (prUpdateDTO.getActualValue() != null) {
                _pr.setActualValue(prUpdateDTO.getActualValue());
            }

            return new ResponseEntity<>(performanceRecordRepository.save(_pr), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{sid}/{year}")
    public ResponseEntity<HttpStatus> deletePerformanceRecord
            (
                    @PathVariable("sid") int sid,
                    @PathVariable("year") int year
            ) {

        Optional<Salesman> salesmanData = salesmanRepository.findBySid(sid);

        if (salesmanData.isPresent()) {
            try {
                performanceRecordRepository.deleteBySidAndYear(sid, year);
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
